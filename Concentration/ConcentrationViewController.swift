//
//  ViewController.swift
//  Concentration
//
//  Created by hh on 2018/10/23.
//  Copyright © 2018 hh. All rights reserved.
//

import UIKit

class ConcentrationViewController: UIViewController {
    
    lazy var game = Concentration(numberOfPairsOfCards: (cardButtons.count+1)/2)
    
    var flipCount = 0 {
        didSet {
            let string = traitCollection.verticalSizeClass == .compact ? "Flips\n\(flipCount)" : "Flips: \(flipCount)"
            flipCountLabel.text = string
        }
    }
    
//    var emojiChoices = ["😇","☢️","😈","👻","👽","👹","🎃"]
        var emojiChoices = "😇☢️😈👻👽👹🎃"
    
    var theme : String? {
        didSet {
            emojiChoices = theme ?? ""
            emoji=[:]
            updateViewFromModel()
        }
    }
    
//    var emoji : Dictionary<Int,String> = [Int:String]()
    
    var emoji = [Card:String]()
    
    @IBAction func testCodeSnippet(_ sender: UIButton) {

        testABC()
        
        print("there're  \(view.contentScaleFactor) pixels per point" )
        
    }
    
    
    @IBOutlet weak var flipCountLabel: UILabel!
    
    @IBOutlet var cardButtons: [UIButton]!
    
    var visibileCardButtons:[UIButton]! {
        return cardButtons?.filter { !$0.superview!.isHidden }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewFromModel()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        let string = traitCollection.verticalSizeClass == .compact ? "Flips\n\(flipCount)" : "Flips: \(flipCount)"
        flipCountLabel.text = string
    }
    
    @IBAction func touchCard(_ sender: UIButton) {
        
        flipCount+=1
        if let cardNumber = cardButtons.index(of: sender) {
            game.chooseCard(at: cardNumber)
            updateViewFromModel()
        } else {
            print("chosen card not in card buttons")
        }
    }
    
    
    func updateViewFromModel() {
        if cardButtons != nil {
            for index in cardButtons.indices {
                let button = cardButtons[index]
                let card = game.cards[index]
                if card.isFacedUp {
                    button.setTitle(emoji(for: card), for: UIControl.State.normal)
                    button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                } else {
                    button.setTitle("", for: UIControl.State.normal)
                    button.backgroundColor = card.isMatched ?  #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0) : #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
                }
                
            }
        }
        
    }
    
    func emoji(for card: Card) -> String {
        if emoji[card] == nil , emojiChoices.count > 0 {
//            emoji[card] = emojiChoices.remove(at: emojiChoices.count.random)
            emoji[card] = String(emojiChoices.remove(at: emojiChoices.index(emojiChoices.startIndex, offsetBy: emojiChoices.count.random)))
        }
        print("emoji for card\(card):\(emoji[card]!)")
        return emoji[card] ?? "?"
    }

}

extension Int {
    var random:Int {
        if (self > 0 ) {
            return Int(arc4random_uniform(UInt32(self)))
        } else if (self < 0) {
            return -Int(arc4random_uniform(UInt32(abs(self))))
        } else {
            return 0
        }
    }
}
