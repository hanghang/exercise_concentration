//
//  Test.swift
//  Concentration
//
//  Created by hh on 2018/10/26.
//  Copyright © 2018 hh. All rights reserved.
//

import Foundation

class A {
    var b:B?
    var c:C?
    init() {
        print("a is being initialized")
    }
    deinit {
        print("a is being deinitialized")
    }
}

class B {
    weak var a:A?
    init() {
        print("b is being initialized")
    }
    deinit {
        print("b is being deinitialized")
    }
}

class C {
    unowned var a:A
    init(_ a:A) {
        self.a = a
        print("c is being initialized")
    }
    deinit {
        print("c is being deinitialized")
    }
}

func testABC() {
    var a : A? = A()
    var b : B? = B()
    
    a!.b = b
    b!.a = a
    
    a!.c = C(a!)
    
    print("setting a to nil")
    a = nil
    print("setting b to nil")
    b = nil
}

