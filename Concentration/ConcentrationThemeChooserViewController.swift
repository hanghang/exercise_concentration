//
//  ConcentrationThemeChooserViewController.swift
//  Concentration
//
//  Created by hh on 2018/11/5.
//  Copyright © 2018 hh. All rights reserved.
//

import UIKit

class ConcentrationThemeChooserViewController: UIViewController, UISplitViewControllerDelegate {


    let themes = [
        "Sports":"⚽️🏀🏈⚾️🎾🏐🏉🎱🏓🏹⛳️🎣",
        "Animals":"🦖🦕🐙🦑🐶🐱🐭🦊🐻🐵🐥🐼",
        "Faces":"😇😈👽👹😱😂😍😎🤪🤬😓🤡😘",
    ]
    
    let id_choose_theme = "choose theme"
    
    private var splitViewDetailConcentrationViewController:ConcentrationViewController? {
        return splitViewController?.viewControllers.last as? ConcentrationViewController
    }
    
    @IBAction func changeTheme(_ sender: Any) {
        if let cvc = splitViewDetailConcentrationViewController {
            if let themeName = (sender as? UIButton)?.currentTitle, let theme = themes[themeName] {
                cvc.theme = theme
            }
        } else if let cvc = lastSeguedToConcentrationViewController {
            if let themeName = (sender as? UIButton)?.currentTitle, let theme = themes[themeName] {
                cvc.theme = theme
            }
            navigationController?.pushViewController(cvc, animated: true)
        } else {
            performSegue(withIdentifier: id_choose_theme, sender: sender)
        }
    }
    
    private var lastSeguedToConcentrationViewController: ConcentrationViewController?
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == id_choose_theme {
            if let themeName = (sender as? UIButton)?.currentTitle, let theme = themes[themeName]{
                if let cvc = segue.destination as? ConcentrationViewController {
                    cvc.theme = theme
                    lastSeguedToConcentrationViewController = cvc
                }
            } else {
                print("no theme found")
            }
        } else {
            print("segue.identifier=\(segue.identifier ?? "?")")
        }
        
    }
    
//    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
//        print("set perform segue to true")
//        return true
//    }
    
    override func awakeFromNib() {
        splitViewController?.delegate = self
    }
    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        if let cvc = secondaryViewController as? ConcentrationViewController {
            print("is cvc's theme nil? \(cvc.theme == nil)")
            if cvc.theme == nil {
                return true
            }
        }
        return false
    }
    
}
