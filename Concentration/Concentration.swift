//
//  Concentration.swift
//  Concentration
//
//  Created by hh on 2018/10/23.
//  Copyright © 2018 hh. All rights reserved.
//

import Foundation
class Concentration {
    
    var cards = [Card]()
    
    private var indexOfOneAndOnlyFaceUpCard:Int? {
        get {
            let faceUpCardIndices = cards.indices.filter { cards[$0].isFacedUp }
            return faceUpCardIndices.oneAndOnly
        }
//        set {
//            for index in cards.indices {
//                cards[index].isFacedUp = (index == newValue)
//            }
//        }
    }
    
    func chooseCard(at index:Int) {
        
        if !cards[index].isMatched {
            if let matchIndex = indexOfOneAndOnlyFaceUpCard, matchIndex != index {
                // check if cards match
                if cards[index] == cards[matchIndex] {
                    cards[index].isMatched = true
                    cards[matchIndex].isMatched = true
                }
                cards[index].isFacedUp=true
//                indexOfOneAndOnlyFaceUpCard = nil
            } else {
                // either no cards or 2 cards are face up
                for i in cards.indices {
                    cards[i].isFacedUp = false
                }
                cards[index].isFacedUp = true
//                indexOfOneAndOnlyFaceUpCard = index
            }
        }

    }
    
    init(numberOfPairsOfCards: Int) {
        
        for _ in 1 ... numberOfPairsOfCards {
            let card = Card()
            cards += [card,card]
        }
        
        // TODO: Shuffle the cards
        
        cards.shuffle()
    }
}

extension Collection {
    var oneAndOnly: Element? {
        return count == 1 ? first : nil
    }
}
