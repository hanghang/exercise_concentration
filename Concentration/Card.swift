//
//  Card.swift
//  Concentration
//
//  Created by hh on 2018/10/23.
//  Copyright © 2018 hh. All rights reserved.
//

import Foundation

struct Card: Hashable
{
    
    var isFacedUp = false
    var isMatched = false
    private var identifier: Int
    
    var hashValue :Int {
        return identifier
    }
    
    static func ==(lhs:Card, rhs:Card) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    
    static var identifierFacotry = 0
    static func getUniqueIdentifier() -> Int {
        identifierFacotry += 1
        return identifierFacotry
    }
    
    init () {
        self.identifier = Card.getUniqueIdentifier()
    }
}


